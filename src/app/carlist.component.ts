import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';

import { Car } from './car';
import { CarService } from './car.service';

@Component({
    selector: 'carlist',
    templateUrl: './carlist.component.html',
    styleUrls: ['./carlist.component.css']
})
export class CarListComponent implements OnInit {

    cars: Car[] = [];
    currentPage: number;
    hasNextPage: boolean = false;

    constructor(
        private carService: CarService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.currentPage = 1;
        
        this.updateCarsInfo();
    }

    editCar(car: Car) {
        this.router.navigate(['addeditcar']);
    }

    deleteCar(carId: string) {
        var confirm = window.confirm("Delete car with id: " + carId + "?");
        
        if (confirm) {
            this.carService.removeCar(carId);

            this.updateCarsInfo();
        }
    }

    setPage(pageId: number) {
        this.currentPage = pageId;
        
        this.updateCarsInfo();
    }

    updateCarsInfo() {
        var carInfo = this.carService.getCarsOnPage(this.currentPage);
        
        this.cars = carInfo.cars;
        
        this.hasNextPage = carInfo.hasNextPage;
    }
}