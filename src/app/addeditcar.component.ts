import { Component, OnInit }                    from '@angular/core';
import { Location }                             from '@angular/common';
import { ActivatedRoute, Router }               from '@angular/router';
import { FormGroup, FormControl, Validators }   from '@angular/forms';

import { Car, FuelType, Transmission }          from './car';
import { CarService }                           from './car.service';

@Component({
    selector: 'addeditcar',
    templateUrl: './addeditcar.component.html',
    styleUrls: ['./addeditcar.component.css']
})
export class AddEditCarComponent implements OnInit {

    car : Car = new Car();
    showEditCar : boolean = false;
    brand : FormControl;
    model : FormControl;
    description : FormControl;
    regNum : FormControl;
    fuelType : FormControl;
    transmission : FormControl;
    addEditCarForm : FormGroup;

    constructor(
        private carService: CarService,
        private location: Location,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.processRouteParams();

        this.createFormControls();
        this.createForm();
    }

    processRouteParams() {
        var carId;
        
        this.route.params.subscribe(params => {
            carId = +params['id'];
        });

        if (carId) {
            this.car = this.carService.getCar(carId);

            if (this.car) {
                this.showEditCar = true;
            } else {
                this.showEditCar = false;

                this.car = new Car();
            }
        } else {
            this.showEditCar = false;

            this.car = new Car();
        }
    }

    createFormControls() {
        this.brand = new FormControl('', Validators.required);
        this.model = new FormControl('', Validators.required);
        this.description = new FormControl('');
        this.regNum = new FormControl('', Validators.required);
        this.fuelType = new FormControl('');
        this.transmission = new FormControl('');
    }

    createForm() {
        this.addEditCarForm = new FormGroup({
            brand : this.brand,
            model : this.model,
            description : this.description,
            regNum : this.regNum,
            fuelType : this.fuelType,
            transmission : this.transmission
          });
    }

    addCar(newCar: Car) {
        if (newCar.brand && newCar.model && newCar.registrationNumber) {
            this.carService.addCar(newCar);

            newCar.brand = "";
            newCar.model = "";
            newCar.description = "";
            newCar.registrationNumber = "";
            newCar.fuelType = FuelType.Petrol;
            newCar.transmission = Transmission.Manual;

            this.router.navigate(['carlist']);
        } else {
            alert("Fill in all mandatory fields!");
        }
    }

    updateCar(car: Car) {
        if (car.brand && car.model && car.registrationNumber) {
            this.carService.updateCar(car);
            
            this.router.navigate(['carlist']);
        } else {
            alert("Fill in all mandatory fields!");
        }
    }

    cancel() {
        this.location.back();
    }
}