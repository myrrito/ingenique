import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CarListComponent }   from './carlist.component';
import { AddEditCarComponent }      from './addeditcar.component';

const routes: Routes = [
    { path: '', redirectTo: '/carlist', pathMatch: 'full' },
    { path: 'carlist', component: CarListComponent },
    { path: 'addeditcar/:id', component: AddEditCarComponent }, // used to edit a car
    { path: 'addeditcar',     component: AddEditCarComponent } // used to add a car
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}