const FuelType = {
    Petrol: "Petrol" as "Petrol",
    Diesel: "Diesel" as "Diesel",
    Electric: "Electric" as "Electric"
}

type FuelType = (typeof FuelType)[keyof typeof FuelType];
export { FuelType };

const Transmission = {
    Manual: "Manual" as "Manual",
    Automatic: "Automatic" as "Automatic",
    SemiAutomatic: "Semi-automatic" as "Semi-automatic"
}

type Transmission = (typeof Transmission)[keyof typeof Transmission];
export { Transmission };

export class Car {

    constructor() { 
        this.fuelType = FuelType.Petrol;
        this.transmission = Transmission.Manual;
    }
    
    id: number;
    brand: string;
    model: string;
    description: string;
    registrationNumber: any;
    fuelType: FuelType;
    transmission: Transmission;
  }