import { BrowserModule }        from '@angular/platform-browser';
import { NgModule }             from '@angular/core';
import { FormControl,
         FormGroup,
         FormsModule,
         ReactiveFormsModule }  from '@angular/forms';

import { AppComponent }         from './app.component';
import { AddEditCarComponent}   from './addeditcar.component';
import { CarListComponent }     from './carlist.component';
import { CarService }           from './car.service';

import { AppRoutingModule }     from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    AddEditCarComponent,
    CarListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
