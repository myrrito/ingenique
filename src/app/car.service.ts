import { Car } from './car';
import { Injectable } from '@angular/core';

//@Injectable()
export class CarService {
    CARS_LOCAL_STORAGE_KEY : string = "IngeniqueCars";
    CARS_PER_PAGE : number = 3;

    constructor() { }
    
    /**
     * Returns a list of all saved cars
     */
    getCars() {
        var carJSONString = localStorage.getItem(this.CARS_LOCAL_STORAGE_KEY),
            carList;

        if (!carJSONString) {
            return [];
        }

        carList = JSON.parse(carJSONString);

        return carList;
    }

    /**
     * Returns an object containing a list of cars for page {pageId} and if there is next page
     * @param pageId The page for which we want to see the cars
     */
    getCarsOnPage(pageId: number) {
        var cars = this.getCars(),
            start, end;
        
        var result: {[k: string]: any} = {};

        start = this.CARS_PER_PAGE * (pageId - 1);
        end = this.CARS_PER_PAGE * pageId;

        if (end <= cars.length - 1) {
            result.hasNextPage = true;
        } else {
            result.hasNextPage = false;
        }
        
        cars = cars.slice(start, end);

        result.cars = cars;

        return result;
    }

    getCar(carId: string) {
        var carList = this.getCars(),
            i;

        for(i = 0; i < carList.length; i++) {
            if (carList[i].id === carId) {
                return carList[i];
            }
        }

        return null;
    }

    addCar(car: Car) {
        var carList = this.getCars(),
            returnValue;

        if (carList.length === 0) {
            car.id = 1;
        } else {
            car.id = carList[carList.length - 1].id + 1; // increment the last car's id
        }

        carList.push(car);

        localStorage.setItem(this.CARS_LOCAL_STORAGE_KEY, JSON.stringify(carList));
    }

    removeCar(carId: string) {
        var carList = this.getCars(),
            deletedCar = {},
            i;

        for (i = 0; i < carList.length; i++) {
            if (carList[i].id === carId) {
                deletedCar = carList.splice(i, 1)[0];

                localStorage.setItem(this.CARS_LOCAL_STORAGE_KEY, JSON.stringify(carList));

                break;
            }
        }

        return deletedCar;
    }

    updateCar(car: Car) {
        var carList = this.getCars(),
            i;

        for (i = 0; i < carList.length; i++) {
            if (carList[i].id === car.id) {
                carList[i] = car;

                localStorage.setItem(this.CARS_LOCAL_STORAGE_KEY, JSON.stringify(carList));
                
                return true; // update succeeded
            }
        }

        return false;
    }
}